from flask import Flask, request, jsonify

app = Flask(__name__)

@app.route('/api/multiple', methods=['GET'])
def multiply():
    # Extract query parameters
    x1 = request.args.get('x1', type=int)
    x2 = request.args.get('x2', type=int)

    # Perform multiplication
    if x1 is not None and x2 is not None:
        result = x1 * x2
        return jsonify(result=result)
    else:
        return jsonify(error="Invalid input"), 400

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=8082)
