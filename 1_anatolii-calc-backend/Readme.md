## 1 - Create files

```commandline

mkdir anatolii-calc-backend
cd anatolii-calc-backend
touch app.py
touch Dockerfile
python3 -m venv venv
source venv/bin/activate
pip install Flask

```


## 2 - Dockerfile, app.py

insert necessary data inside


## 3 - run it locally

#### 3.1 - Install 'colima' and others
brew install docker
brew install docker-compose
brew install colima
colima start --with-kubernetes



### 3.2 - Activate virtual env & build docker image: 
source venv/bin/activate
docker build -t dreamnewq/anatolii-calc-backend .


### 3.3 - Run it locally from Dockerfile

docker run -p 8082:8082 dreamnewq/anatolii-calc-backend
in browser:
    http://localhost:8082/api/multiple?x1=10&x2=4
    http://localhost:8082/multiple?x1=10&x2=4
Ctrl+C


### 3.4 Push dckerimage to DockerHub
docker login

// if needed, 1 time, for credentials:
// echo "{}" > ~/.docker/config.json

docker push dreamnewq/anatolii-calc-backend

========
## Very short - to update:

docker build -t dreamnewq/anatolii-calc-backend .
docker push dreamnewq/anatolii-calc-backend
