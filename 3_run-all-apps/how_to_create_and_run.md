touch docker-compose-calc.yaml

...
## 1 - Start:

```commandline
docker-compose -f docker-compose-calc.yaml up -d
```

## 2 - Stop:

```commandline
docker-compose -f docker-compose-calc.yaml down
```

### To debug/troubleshouting


see logs:
    docker-compose -f docker-compose-calc.yaml logs traefik 



Traefik web interface (open in browser):
http://localhost:8080


====
to install curl inside docker container:

BACKEND LOGS:
docker exec -it 3_run-all-apps-anatolii-calc-backend-1 /bin/sh
UI LOGS:
docker exec -it 3_run-all-apps-anatolii-calc-ui-1 /bin/sh
TRAEFIK LOGS:
docker-compose -f docker-compose-calc.yaml logs traefik -f


apt-get update && apt-get install -y curl
# curl "http://localhost:8082/api/multiple?x1=10&x2=5"
{"result":50}
# 
