## 1 - Create file structure

mkdir anatolii-calc-ui
cd anatolii-calc-ui
touch index.html
touch Dockerfile

## 2 - Provide content

## 3 - create docker image, verify it works:

docker build -t dreamnewq/anatolii-calc-ui .

docker run -p 8080:80 dreamnewq/anatolii-calc-ui

http://localhost:8080

Ctrl + C


# in very short, next iteration:

docker build -t dreamnewq/anatolii-calc-ui .
docker push dreamnewq/anatolii-calc-ui
